start_node_tunnels () {
    # give it the nodes to make tunnels to in order of the jobs

    nodes=("${@}")

    echo "Killing existing tunnels"
    pkill -e -f 'ssh -N -F /home/salotz/.ssh/config -J hpcc.dev'

    echo "Getting info for: $nodes"

   for i in ${!nodes[@]}; do

       node="${nodes[$i]}"

       # pad with zeros so we can do more than 10 tunnels
       idx=$(printf "%02d\n" $i)

       if [[ "$node" == 'None' ]]; then

           echo "No node for job $idx, skipping"
       else
           echo "job: $idx JOBID: $node"

           tunn_command="$(cat << EOF
           autossh \
               -M 501${idx} \
               -- \
               -N \
               -F $HOME/.ssh/config -J hpcc.dev \
               -L 50${idx}0:localhost:9100 \
               -L 50${idx}1:localhost:9445 \
               -L 50${idx}2:localhost:9001 \
               lotzsamu@$node
EOF
)"

           echo "Running this command:"
           echo $tunn_command

           $tunn_command &

           echo "PID: $!"
       fi

   done

   echo "Processes"
   echo "----------------------------------------"
   echo $(ps aux | grep 'ssh -N -F /home/salotz/.ssh/config -J hpcc.dev')
}
