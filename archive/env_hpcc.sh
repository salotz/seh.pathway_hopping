#!/bin/sh

# a script to set up the environment properly

conda deactivate
env_name="seh_pathway_hopping"
env_path="$HOME/anaconda3/envs/${env_name}"
pinned_path="${env_path}/conda-meta/pinned"

# clear the pinned file, just in case
echo "" >> "$pinned_path"

conda env remove -y -n "$env_name"
conda create -y -n "$env_name" python=3.6
conda activate "$env_name"

# pin the dependencies into the conda-meta file for this env
echo "distributed 2.*" >> "${pinned_path}"

# msmbuilder build and runtime dependencies
echo "cython ==0.27.3" >> "${pinned_path}"
echo "mdtraj ==1.8" >> "${pinned_path}"

# install all things
conda install -y -c 'conda-forge' \
      numpy pandas scipy networkx h5py matplotlib scikit-learn \
      dask 'distributed>=2.0' dask-jobqueue \
      ipython cython mdtraj sqlalchemy prefect joblib pdbpp \
      numpydoc pytables pyhmc pyyaml jinja2 fastcluster

# install custom msmbuilder repo
#pip install git+https://github.com/salotz/msmbuilder@distance

conda install --freeze-installed -y -c omnia openmm
pip install -r requirements.txt
pip install -e "$HOME/devel/wepy" --no-use-pep517
pip install -e "$HOME/devel/seh.pathway_hopping"
pip install git+https://github.com/salotz/msmbuilder@distance
