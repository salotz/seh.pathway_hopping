"""Put user defined tasks in the plugins folder. You can start with
some customizations in this file which is included by default."""

from invoke import task
from invocations.console import confirm

from ..config import *

from ..modules import org as org_tasks

import itertools as it


@task(pre=[org_tasks.clean])
def org_clean(ctx):
    """Clean org tangle products specific to this project."""

    # specify extensions so we don't remove the .keep file (TODO
    # probably a better way to do this)

    # prep
    ctx.run("rm -f prep/*.sh")
    ctx.run("rm -f prep/*.bash")
    ctx.run("rm -f prep/*.xsh")
    ctx.run("rm -f prep/*.py")

    # hpcc scripts
    ctx.run("rm -f hpcc/scripts/*.sh")
    ctx.run("rm -f hpcc/scripts/*.bash")
    ctx.run("rm -f hpcc/scripts/*.xsh")
    ctx.run("rm -f hpcc/scripts/*.py")

    # templates
    ctx.run("rm -f hpcc/templates/*.j2")

# HPCC job generation stuff
@task
def init_lig_sim_dir(cx, lig=None):

    if lig is None:
        lig_ids = LIG_IDS
    else:
        lig_ids = [lig]

    for lig_id in lig_ids:
        sim_dir = f"{PROJECT_DIR}/hpcc/simulations/{lig_id}_simulations"
        cx.run(f'mkdir -p "{sim_dir}"')

        cx.run(f'mkdir -p "{sim_dir}/logs"')
        cx.run(f'touch "{sim_dir}/logs/.keep"')

        cx.run(f'mkdir -p "{sim_dir}/submissions"')
        cx.run(f'touch "{sim_dir}/submissions/.keep"')

        cx.run(f'mkdir -p "{sim_dir}/tasks"')
        cx.run(f'touch "{sim_dir}/tasks/.keep"')

        # create the resource directories
        res_dir = f"{RESOURCE_DIR}/hpcc/simulations/{lig_id}_simulations"
        cx.run(f'mkdir -p "{res_dir}/configurations"')
        cx.run(f'mkdir -p "{res_dir}/input"')
        cx.run(f'mkdir -p "{res_dir}/orchs"')
        cx.run(f'mkdir -p "{res_dir}/results"')

        # link to the resource directories
        cx.run(f'ln -r -s -f -T "{res_dir}/configurations" "{sim_dir}/configurations"')
        cx.run(f'ln -r -s -f -T "{res_dir}/input" "{sim_dir}/input"')
        cx.run(f'ln -r -s -f -T "{res_dir}/orchs" "{sim_dir}/orchs"')
        cx.run(f'ln -r -s -f -T "{res_dir}/results" "{sim_dir}/results"')


@task(pre=[org_tasks.tangle])
def gen_configurations(cx):

    work_mapper_types = ['WorkerMapper', 'TaskMapper',]
    # we only need to specify one of the GPU platforms since they use
    # the same classes
    platforms = ['CPU', 'CUDA',]

    # this is not really useful to have be specific since it is always set at runtime
    num_workers = 1

    work_mapper_specs = (('Mapper', 'None') +
                         list(it.product(work_mapper_types, platforms)))

    for work_mapper_type, platform in work_mapper_specs:

        cx.run(f"bash -i {PROJECT_DIR}/prep/gen_configuration.sh {work_mapper_type} {platform} {num_workers}")

        #cx.run(f"bash -i {PROJECT_DIR}/prep/gen_configuration.sh {worker_type} {num_workers}")

@task
def clean_configurations(cx):

    for lig_id in LIG_IDS:
        cx.run(f'rm -rf "{PROJECT_DIR}/data/configurations/{lig_id}"/*')
        cx.run(f'rm -rf "{PROJECT_DIR}/hpcc/simulations/{lig_id}_simulations/input"/*')
        cx.run(f'rm -rf "{PROJECT_DIR}/hpcc/simulations/{lig_id}_simulations/configurations"/*')


@task(pre=[org_tasks.tangle])
def hpcc_gen_tasks(c):

    for lig_id in LIG_IDS:
        print("Generating tasks for Ligand {}".format(lig_id))

        sim_dir = './hpcc/simulations/{}_simulations'.format(lig_id)

        # remove the exisiting tasks
        c.run('rm -f "{}/tasks/*"'.format(sim_dir))

        # regenerate the scripts
        c.run('python ./hpcc/scripts/gen_tasks.py '
              './hpcc/templates {sim_dir}/tasks {sim_dir}/run_spec.toml '.format(
            sim_dir=sim_dir))


@task()
def hpcc_clean_tasks(cx):

    for lig_id in LIG_IDS:

        print("removing tasks for: {}".format(lig_id))

        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)

        # remove the exisiting tasks
        cx.run('rm -f "{}"/tasks/*'.format(sim_dir))

@task(pre=[org_tasks.tangle, hpcc_gen_tasks])
def hpcc_gen_submissions(c, tag=None):

    for lig_id in LIG_IDS:
        print("Generating submissions for Ligand {}".format(lig_id))

        if tag is not None:
            label = "lig_{}_sims_{}".format(lig_id, tag)
        else:
            label = "lig_{}_sims".format(lig_id)

        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)

        command = \
"""slurmify \
        --config ./hpcc/run_settings.toml \
        --context {sim_dir}/context_settings.toml \
        --batch-in {sim_dir}/tasks \
        --batch-out {sim_dir}/submissions \
        {label}
""".format(
    sim_dir=sim_dir, label=label)

        # regenerate the scripts
        c.run(command)

@task()
def hpcc_clean_submissions(cx):

    for lig_id in LIG_IDS:


        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)
        print("removing submissions: {}".format(sim_dir))

        # remove the exisiting tasks
        cx.run('rm -f "{}"/submissions/*'.format(sim_dir))

@task
def hpcc_test_submit(cx):

    for lig_id in LIG_IDS:

        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)
        print("Submitting test run for: {}".format(sim_dir))

        # the sbatch command must be run from the simulation directory
        # since it is context sensitive
        command =\
'''(cd {sim_dir}; sbatch submissions/lig-{lig_id}_test.sh.slurm)
'''.format(
    lig_dir=lig_id,
    sim_dir=sim_dir
)
        # remove the exisiting tasks
        cx.run(command)


@task(pre=[hpcc_gen_tasks])
def hpcc_test_local(cx, lig=None):

    if lig is None:
        lig_ids = LIG_IDS
    else:
        lig_ids = [lig]

    for lig_id in lig_ids:

        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)
        print("Running local test run for: {}".format(sim_dir))

        task_script = "{}/tasks/lig-{}_test-local.sh".format(sim_dir, lig_id)

        test_job_dir = '{}/test_jobs/test'.format(sim_dir)
        inputs_dir = '{}/input'.format(sim_dir)

        # make the job directory, cleaning it if it had stuff
        cx.run('mkdir -p "{}"'.format(test_job_dir))
        cx.run('rm -rf "{}"/*'.format(test_job_dir))

        # copy the inputs to the directory
        cx.run('cp "{}"/* "{}"/'.format(inputs_dir, test_job_dir))

        command =\
'''(cd "{job_dir}"; {task})
'''.format(
    task=task_script,
    job_dir=test_job_dir,
)

        print(command)
        if confirm("Run this command?"):
            cx.run(command)



@task
def hpcc_clean_tests(cx):

    for lig_id in LIG_IDS:

        sim_dir = '{}/hpcc/simulations/{}_simulations'.format(PROJECT_DIR, lig_id)
        print("Cleaning tests for: {}".format(sim_dir))

        cx.run('rm -rf "{sim_dir}"/test_jobs')

@task(hpcc_gen_submissions)
def hpcc_submit(cx):
    # TODO
    pass

@task(org_tasks.tangle)
def push_scripts(cx):

    target_files = [
        'project.org',
        'tools.requirements.txt',
        'setup.py',
        'env.bash',
    ]

    target_dirs = [
        'src',
        'tasks',

        # the tangled things
        'configs',
        'scripts',
        'troubleshoot',
        'hpcc/scripts',
        'hpcc/templates',
    ]

    for targ_f in target_files:

        # the project file
        command = \
                  f"""rsync -ravvhhiz \
                  {PROJECT_DIR}/{targ_f} \
                  lotzsamu@rsync.hpcc.msu.edu:{ICER_SCRATCH_DIR}/
                  """
        cx.run(command)

    for targ_d in target_dirs:

        # the tools requirements file
        command = \
                  f"""rsync -ravvhhiz \
                  {PROJECT_DIR}/{targ_d}/ \
                  lotzsamu@rsync.hpcc.msu.edu:{ICER_SCRATCH_DIR}/{targ_d}
                  """
        cx.run(command)

@task
def hpcc_clean_scripts(cx):

    # prep scripts
    cx.run(f'rm -rf "{PROJECT_DIR}"/prep/*')
    cx.run(f'touch "{PROJECT_DIR}"/prep/.keep')

    # hpcc/script scripts
    cx.run(f'rm -rf "{PROJECT_DIR}"/hpcc/scripts/*')
    cx.run(f'touch "{PROJECT_DIR}"/hpcc/scripts/.keep')


@task(pre=[hpcc_clean_tasks,
           hpcc_clean_submissions,
           hpcc_clean_tests,
           hpcc_clean_scripts])
def hpcc_clean(cx):
    pass

