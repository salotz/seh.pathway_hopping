"""User settings for a project."""

# load the system configuration. You can override them in this module,
# but beware it might break stuff
from .sysconfig import *



### Imports
import os.path as osp
import platform

### Project Metadata

# the name of the project
PROJECT_NAME = "seh.pathway_hopping"

# the url compatible name of the project
PROJECT_SLUG = "seh_pathway_hopping"

# copyright options:
# https://en.wikipedia.org/wiki/Creative_Commons_license#Seven_regularly_used_licenses
# CCO, BY, BY-SA, BY-NC, BY-NC-SA, BY-ND, BY-NC-ND

# copyright of the project
PROJECT_COPYRIGHT = "BY-NC"

## Project Owner

# real name of the owner of the project
OWNER_NAME = "Samuel D. Lotz"

# email of the owner
OWNER_EMAIL = "samuel.lotz@salotz.info"

# handle/username/nickname of the owner
OWNER_NICKNAME = "salotz"

## Environment information

# which shell profile to use
SHELL_PROFILE = "common"

# the subdomain of tree this project is in
REFUGUE_DOMAIN = 'lab'

## Updating

# where to download git repos to; the "cookiejar"
COOKIEJAR_DIR = "$HOME/tmp/cookiejar"

# URL to get updates from
UPDATE_URL="https://github.com/salotz/analytics-cookiecutter.git"

### Project Configuration

# since this get's run on different hosts I dynamically choose project
# dirs

# "/mnt/gs18/scratch/users/lotzsamu/tree/lab/projects/wepy.lysozyme_test"
ICER_SCRATCH_DIR = "/mnt/gs18/scratch/users/lotzsamu/tree/lab/projects/{}".format(PROJECT_NAME)
ICER_SCRATCH_RESOURCE_DIR = "/mnt/gs18/scratch/users/lotzsamu/tree/lab/resources/project-resources/{}".format(PROJECT_NAME)

LOCAL_DIR = osp.expandvars("$HOME/tree/lab/projects/{}".format(PROJECT_NAME))
LOCAL_RESOURCE_DIR = osp.expandvars(
    "$HOME/tree/lab/resources/project-resources/{}".format(PROJECT_NAME))

# choose the root paths
if platform.node() in ['ostrich', 'superior',]:
    PROJECT_DIR = LOCAL_DIR
    RESOURCE_DIR = LOCAL_RESOURCE_DIR

elif platform.node() in [
        'dev-intel16-k80',
        'dev-intel18',
        'dev-intel16',
        'dev-intel14',
        'gateway-03',
        'gateway-02',
        'gateway-01',
        'gateway-00',
        'globus-01', # rsync.hpcc.msu.edu
        'globus-00',
]:
    # then use scratch
    PROJECT_DIR = ICER_SCRATCH_DIR
    RESOURCE_DIR = ICER_SCRATCH_RESOURCE_DIR

# directories which will be assumed to exist, these will be built with
# the "init" command
PROJECT_DIRS = [
    'src',
    'scripts',
    'data',
    'cache',
    'db',
    'tmp',
    'scratch',
    'troubleshoot',
]


### Environments

PY_ENV_NAME = 'seh.pathway_hopping.project'
PY_VERSION = '3.7'

### VCS

## Git

# specify the directories which should be stored in git-lfs
GIT_LFS_TARGETS = [
    'media/*',
]


### Packaging

## Python


### Resources

RESOURCES = [
    'cache',
    'data',
    'db',
]



### PROJECT SPECIFIC STUFF

LIG_IDS = [3, 10, 17, 18, 20]
"""The ligand ids for all separate ligand systems we consider here."""

RES_ID = "UNL"
"""This is the name for the residue of the ligand in all of the
topology representations"""

SEG_ID = "HETA"
"""This is the name for the segment designation of the ligand in the
the topologies that support this."""
