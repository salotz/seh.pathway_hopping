#!/bin/bash --login

source $HOME/.bashrc

ANACONDA_DIR="$HOME/anaconda3"

# use the ANACONDA_DIR which is set in the environment variables in
# the setup to setup this shell for anaconda
. ${ANACONDA_DIR}/etc/profile.d/conda.sh

# a script to set up the environment properly

conda deactivate
env_name="seh_pathway_hopping"
env_path="$HOME/anaconda3/envs/${env_name}"
pinned_path="${env_path}/conda-meta/pinned"

# clear the pinned file, just in case
echo "" >> "$pinned_path"

conda env remove -y -n "$env_name"
conda create -y -n "$env_name" python=3.7
conda activate "$env_name"

# pin the dependencies into the conda-meta file for this env
echo "distributed 2.*" >> "${pinned_path}"

# install all things
conda install -y -c 'conda-forge' \
      numpy pandas scipy networkx h5py matplotlib scikit-learn \
      dask 'distributed>=2.0' dask-jobqueue \
      ipython cython mdtraj sqlalchemy prefect joblib pdbpp

# always specify the openmm build you want so it doesn't mysteriously
# break and I have to track it down why, for some reason 10.1 on HPCC
# isn't working so we will use the 10.0 build
conda install --freeze-installed -y -c omnia/label/cuda100 openmm

pip install -r requirements.txt
pip install -e .
