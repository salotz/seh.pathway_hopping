#!/bin/sh  -login 

#SBATCH -e logs/lig_17_sims.%J.err
#SBATCH -o logs/lig_17_sims.%J.out
#SBATCH -J lig_17_sims

#SBATCH --time=168:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4



#SBATCH --mem=200gb





#SBATCH --mail-type="BEGIN,END,FAIL"

#SBATCH --constraint="[intel18|intel16]"

#SBATCH --gres=gpu:8



# ------------------------------

# we print a simple confirmation to the terminal that we have started
echo "Starting job script"

echo "Running the setup"

# ------------------------------

# we print a simple confirmation to the terminal that we have started
echo "Starting job"

# the date and time this was run
date=$(date)

# the job id
jobid=${SLURM_JOB_ID}

# the job name
jobname=${SLURM_JOB_NAME}

# directory path for this task
goal_dir="$SCRATCH/tree/lab/projects/seh.pathway_hopping/hpcc/simulations/17_simulations"

# the directory with all of the input
inputdir=${goal_dir}/input

# the directory with the logs
logsdir=${goal_dir}/logs

# the individual log files
stdout="${logsdir}/${SLURM_JOB_NAME}.${SLURM_JOB_ID}.out"
stderr="${logsdir}/${SLURM_JOB_NAME}.${SLURM_JOB_ID}.err"

# directory to put job results in
jobsdir=${goal_dir}/jobs

# the directory for the output of this job
jobdir=${jobsdir}/${jobid}

# the directory output will be put into after completion
baseoutputdir=${jobdir}/output
outputdir=${baseoutputdir}

# if we are running an interactive job we want to write out the new
# outputs to another outputs folder
count=1
while [ -d "${outputdir}" ]; do
  outputdir="${baseoutputdir}$((count++))";
done

# directory to execute code in

execdir=${jobdir}


# export the execution directory and the job name as a global variable
# so that the epilog can see it


# make these directories if they do not exist
mkdir -p ${jobdir}
mkdir -p ${execdir}
mkdir -p ${outputdir}

echo "Starting Job Log" 
echo "----------------------------------------" 
echo ""   

# print the paths for all the variables constructed in this script
echo "* Path Check" 

echo "" 
echo "JOBID ${jobid}" 
echo "GOAL_DIR ${goal_dir}" 
echo "INPUTDIR ${inputdir}" 
echo "JOBSDIR ${jobsdir}"
echo "JOBNAME ${jobname}" 
echo "JOBDIR ${jobdir}" 
echo "OUTPUTDIR ${outputdir}" 
echo "EXECDIR ${execdir}"
echo "LOGSDIR ${logsdir}" 
echo "STDOUT ${stdout}" 
echo "STDERR ${stderr}" 
echo ""  

# saving SLURM environmental variables
echo "* SLURM Environmental Variables" 

echo ""   
# SLURM env vars
echo "SLURM ENV:"  
env | grep "SLURM"  

# initial
echo "* Environment Initialization" 
echo ""  
echo "----------------------------------------" 
echo "Initialization" 
echo "----------------------------------------" 

# load profile
echo "------------" 
echo "RUNNING: source /etc/profile" 
echo "------------" 
source /etc/profile 
echo "" 

# load hpcc modules
# echo "------------" 
# echo "RUNNING: source /opt/software/modulefiles/setup_modules.sh" 
# echo "------------" 
# source /opt/software/modulefiles/setup_modules.sh 
# echo "" 


# LMOD module load
echo "** Loading Modules" 

echo "loading the module: GNU/4.8.3" 
module load GNU/4.8.3 

echo "loading the module: CUDA/9.2.88" 
module load CUDA/9.2.88 

echo "" 


# set environmental variables and other local variables that are used for 
# many types of scripts
# ===============================================================================
echo "** Setting environmental variables"  


echo "Exporting env var: ANACONDA_DIR"
export ANACONDA_DIR=$HOME/anaconda3


# print out the environmental variables after modifications
echo "** Environmental variables before execution:"  
env 
echo ""   

# ===============================================================================

echo "* Preparing Execution Directory" 
# remove current contents of the execdir, useful for if running
# interactive job which writes to same dir, harmless if not
echo "Removing existing files if they exist in EXECDIR: ${execdir}" 
rm -rf ${execdir}/* 
echo "" 

# copy the input files to the execution directory
echo "------------" 
echo "Copying input files from INPUTDIR: ${inputdir} to EXECDIR: ${execdir}" 
echo "------------" 
cp -rf ${inputdir}/* ${execdir}/ 
echo "" 

# copy the actual submission script used (a self operation)
echo "------------" 
echo "Copying submission script to EXECDIR: $execdir" 
echo "------------" 
cp "${0}" ${execdir}/ 
echo "" 

# copy the epilog if there is one


# copy the task script if there is one

task_script="/home/salotz/tree/lab/projects/seh.pathway_hopping/hpcc/simulations/17_simulations/tasks/.keep"
echo "------------" 
echo "Copying the task script ${task_script} to EXECDIR: ${execdir}" 
echo "------------" 
cp "${task_script}" ${execdir}/ 
echo "" 



# change to the exec dir
echo "------------" 
echo "moving to EXECDIR: ${execdir}" 
echo "------------" 
cd ${execdir} 
echo "" 

# write file names in $execdir to log
echo "------------" 
echo "listing of EXECDIR: $execdir" 
echo "------------" 
ls ${execdir} 
echo "" 


echo ""
# ===============================================================================


# ------------------------------
# The code for this script
# ===============================================================================

# we print a simple confirmation to the terminal that we are starting the main script
echo "Starting main script"

echo "* Script"
echo "------------"
echo "** Running script"
echo "==============================================================================="


srun \
        /home/salotz/tree/lab/projects/seh.pathway_hopping/hpcc/simulations/17_simulations/tasks/.keep



echo "==============================================================================="
echo "* Teardown"
echo "done with script"
echo ""

# ===============================================================================

echo "starting the teardown process"

echo "tearing down"

echo "finished the teardown process"

echo "Exiting."