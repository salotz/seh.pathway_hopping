#!/bin/bash --login
num_gpus=8
n_steps=20000
run_time=590400
checkpoint_freq=25
orch_name="$SCRATCH/tree/lab/projects/seh.pathway_hopping/hpcc/simulations/17_simulations/orchs/master_sEH_lig-17.orch.sqlite"
config_name="TaskMapper_CUDA_8-workers_lig-17_base.config.dill.pkl"
start_hash="bb375a77d0065cdd4e8b86eb05a2cdde"
log_level="DEBUG"
task_name="lig-17_contig-0-1_production"

scripts_dir="$SCRATCH/tree/lab/projects/seh.pathway_hopping/hpcc/scripts"
patches="WepySimApparatus OpenMMRunner.platform_kwargs OpenMMRunner.getState_kwargs UnbindingBC._mdj_top UnbindingBC._periodic WExplorePmin"

# environment stuff
conda_env="$SCRATCH/tree/lab/projects/seh.pathway_hopping/_conda_envs/sims"

gnu_version="GCC/8.3.0"
cuda_version="CUDA/10.1.243"

echo "Modules to start"
module list

echo "purging lmod modules"
module purge

echo "modules after purge"
module list

echo "loading specified lmod modules"
echo "GNU: $gnu_version"
echo "CUDA: $cuda_version"

module load $gnu_version
module load $cuda_version

module list

# functions for exiting on errors
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 1; }
try() { "$@" || die "cannot $*"; }

echo "----------------------"
echo "Running on host: $(hostname)"
echo "----------------------"




# use the ANACONDA_DIR which is set in the environment variables in
# the setup to setup this shell for anaconda
. ${ANACONDA_DIR}/etc/profile.d/conda.sh

echo "DEBUG: before activating the simulation env:"
echo "DEBUG: current 'which python' is:"
echo $(which python)
echo ""

echo "DEBUG: current 'pyenv which python' is:"
echo $(pyenv which python)
echo ""

echo "DEBUG: current 'pyenv which conda' is:"
echo $(pyenv which conda)
echo ""


echo "DEBUG: Trying to activate env: ${conda_env}"
echo ""

# activate the proper virtualenv
try conda activate ${conda_env}

echo "DEBUG: after activating the simulation env."
echo "DEBUG: current 'which python' is:"
echo $(which python)
echo ""

echo "DEBUG: current 'pyenv which python' is:"
echo $(pyenv which python)
echo ""

echo "Testing OpenMM Installation"
python -m simtk.testInstallation


echo ""
echo "Testing wepy installation"

echo "which wepy"
echo $(which wepy)

echo ""

start_snapshot_name="${start_hash}.snap.dill.pkl"

# grab the snapshot out of the master orchestrator, so we have it for
# provenance sake
try wepy get snapshot -O "${start_snapshot_name}" "${orch_name}" "${start_hash}"

# then copy it and rename it so it doesn't have to get overwritten
start_snapshot_backup="${start_snapshot_name}.backup"

try cp $start_snapshot_name $start_snapshot_backup

# apply patches to snapshot
echo "Running patch script for the patches ${patches[@]}"
try python $scripts_dir/patch_snapshot.py $start_snapshot_name $start_snapshot_name ${patches[@]}
echo "Finished with patch script"


## for launching parallel exporters

# only launch right before the main job so we don't have to clean up
# for all the other commands I run above

# this helps us teardown these things upon exit
trap teardown SIGINT

teardown () {
    echo "caught signal tearing processes down"

    kill "$node_exporter_pid"
    echo "Killed node_exporter proc"

    kill "$nvidia_exporter_pid"
    echo "Killed nvidia_exporter proc"

    exit 1
}

# run the node exporters

# TODO get the right path to the exporter binaries

./node_exporter &
node_exporter_pid="$(pidof node_exporter)"
echo "Started the node_exporter as PID: $node_exporter_pid"

./nvidia_gpu_prometheus_exporter &
nvidia_exporter_pid="$(pidof nvidia_gpu_prometheus_exporter)"
echo "Started the nvidia_gpu_prometheus_exporter as PID: $nvidia_exporter_pid"

### run main job

echo "tagging as ${task_name}"

# run the simulation from that snapshot
wepy run snapshot \
     --tag ${task_name} \
     --job-name ${task_name} \
     --n-workers ${num_gpus} \
     --log ${log_level} \
     --checkpoint-freq ${checkpoint_freq} \
     ${start_snapshot_name} \
     ${config_name} \
     ${run_time} ${n_steps} || teardown